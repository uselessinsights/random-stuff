# TOWERS


## IDEA

The set of python scripts for generating various towers in terminal.


## USAGE
`tower-xxx.py [height] [width] [left-margin]`


## EXAMPLE:
$ `python tower-irish.py 10 20 20`

                             /\
                            /  \
                           /    \
                          /      \
                         /        \
                        /          \
                       /            \
                      /              \
                     /                \
                    /                  \
                    ====================
                    |___|___|___/\__|__|
                    |__|___|___|  ||___|
                    |_|___|___||  |___||
                    ||___|___|_|__|__|_|
                    |___|___|___|___|__|
                    |__|___|___|___|___|
                    |_|___|___|___|___||
                    ||___|___|___|___|_|
                    |___|___|___|___|__|
                    |__|___|___|___|___|

## TO BE DONE

* more towers (bell towers, water towers, skyscrapers)
* rewrite in bash (not sure)
