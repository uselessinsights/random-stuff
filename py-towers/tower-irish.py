'''
Script for generating irish round towers.

ARGS:
    tower-irish.py [height] [width] [left-margin]

EXAMPLE:
    $ p tower-irish.py 10 20 20

                             /\
                            /  \
                           /    \
                          /      \
                         /        \
                        /          \
                       /            \
                      /              \
                     /                \
                    /                  \
                    ====================
                    |___|___|___/\__|__|
                    |__|___|___|  ||___|
                    |_|___|___||  |___||
                    ||___|___|_|__|__|_|
                    |___|___|___|___|__|
                    |__|___|___|___|___|
                    |_|___|___|___|___||
                    ||___|___|___|___|_|
                    |___|___|___|___|__|
                    |__|___|___|___|___|

'''


import sys
import random

def cat(*X):
    [print(x, end='') for x in X] \
        if isinstance(X, tuple) \
        else print(X, end='')

H = int(sys.argv[1]) if len(sys.argv) > 1 else 100
W = int(sys.argv[2]) if len(sys.argv) > 2 else 40
P = ' ' * (int(sys.argv[3]) if len(sys.argv) > 3 else 20)

WALL = '|___'
WINDOW = ['|__|', '|  |', '|  |', '_/\\_']


# ROOF -------------------------------------------------------------------------

print()

for i in range(W//2):
    cat(P)
    cat(' ' * (W//2 - 1 - i), '/', ' ' * (2 * i), '\\')
    print()


# ROOF EDGE --------------------------------------------------------------------

cat(P)
cat('=' * W)
print()


# WALLS ------------------------------------------------------------------------

window_counter = 0
for h in range(H):

    cat(P)

    if not window_counter and random.random() > 0.95 and H-h > 3 :
        window_counter = len(WINDOW)
        window_position = random.randint(1, W-5)

    w = 0
    while w < W:
        if w in [0, W-1]:
            cat('|')
        elif window_counter and w == window_position:
            cat(WINDOW[window_counter-1])
            window_counter -= 1
            w += 3
        else:
            cat(WALL[(h+w)%len(WALL)])
        w += 1

    print()
