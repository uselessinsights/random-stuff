# LaTeX Template for University Labs

## IDEA
This LaTeX template simplifies the process of making lab reports.

## SETUP

- Install `Kile`
- Install `texlive-langcyrillic`
- Install `tllocalmgr`
	- Add `adjustbox`
	- Add `collectbox`
	- Add `ucs`

