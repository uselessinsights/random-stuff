\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{/home/alex/Documents/latex/nulp}[2020/03/15 NULP Report]

\LoadClass[14pt,a4paper]{extreport}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PACKAGES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% LANGUAGE
\RequirePackage[T2A]{fontenc}
\RequirePackage[utf8]{inputenc}
\RequirePackage[english, ukrainian]{babel}

% MATH
\RequirePackage{amsmath}

% GEOMETRY
\RequirePackage{geometry}

% IMAGES
\RequirePackage{graphicx}
\RequirePackage[export]{adjustbox}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% COMMANDS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\geometry{left = 3cm}
\geometry{right = 1.5cm}
\geometry{top = 2cm}
\geometry{bottom = 2cm}
\renewcommand{\baselinestretch}{1.25}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% STYLE
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\DeclareRobustCommand{\titleN}[4]{
        \begin{titlepage}
            \centering
                \scshape\normalsize МІНІСТЕРСТВО ОСВІТИ І НАУКИ \par
                \scshape\normalsize НАЦІОНАЛЬНИЙ УНІВЕРСИТЕТ «ЛЬВІВСЬКА ПОЛІТЕХНІКА» \par
            \vspace{1cm}
                \scshape\normalsize Інститут комп'ютерних наук та інформаційних технологій \par
                \scshape\normalsize Кафедра систем штучного інтелекту \par
            \vspace{1.5cm}
                \includegraphics[totalheight=8cm, center]{/home/alex/Documents/latex/img/nulp_logo.jpg} \par
            \vspace{1.5cm}
                \scshape\Large Лабораторна Робота №#1 \par
                \scshape\large з курсу “#2” \par
            \vspace{0.5cm}
                \large Варіант №#3 \par
            \raggedleft\vspace{1.5cm}
                \scshape\small Виконав: \par
                \scshape\small ст. гр. КН-210 \par
                \scshape\small Петровський О. С. \par
            \vspace{0.5cm}
                \scshape\small Викладач: \par
                \scshape\small #4 \par
            \centering\vfill
                \scshape\normalsize Львів - 2020 \par
        \end{titlepage}
}

\DeclareRobustCommand{\imageN}[2]{\includegraphics[totalheight=#1cm, center]{#2}\par}

% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % 

\endinput
