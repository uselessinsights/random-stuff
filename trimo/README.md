# TRIMO [WIP]

## ABOUT

TRIMO (TRee Inspired MOdeling language) is a minimalistic markup language for creating tree-like models of various structures & composite objects using nothing but plain text. It was created under the influence of TOML, YAML and UNIX "tree" shell script, but with the goal of providing a simple way of representing e.g. database structure, REST API endpoints, fields and methods of numerous classes, and so on right in comments of the program, you're working on (or anywhere else). 

I'm planning to create an Atom package, that is able to independently highlight the syntax of TRIMO code blocks, when special shebang-like combination of symbols occurs in comments and implement a bunch of helper functions to easen the process of making notes.

## TO BE DONE

* atom package w/ extension independent syntax highlighting
* conversion traditional formats (JSON, YAML, TOML) 