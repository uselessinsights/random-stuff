module.exports = grammar(
    name: 'trimo'
    rules:

#-------------------------------------------------------------------------------
# ~~% GLOBAL        {OK}

        source_file: ($) -> repeat($._blank)

        _blank: ($) -> choice(
            $.EXP
            $._COMMENT       # {OK}
            $._STRUCT
            $.EOL           # {OK}
        )

        EOL: ($) -> /\n/
        TAB: ($) -> /\s{4}/

# %~
#-------------------------------------------------------------------------------
# ~~% COMMENT         {OK}

        _COMMENT: ($) -> choice(
                $.C_regular
                $.C_header
                $.C_issue
        )

        C_regular: ($) -> /~~[^!?].*/
        C_header: ($) -> /~~\!.*/
        C_issue: ($) -> /~~\?.*/
# %~
#-------------------------------------------------------------------------------
# ~~% EXPRESSION       {OK}

        EXP: ($) -> seq(
            $._VALUE
            $.E_chain
        )

        E_chain: ($) -> seq(
            repeat($.E_task)
            /.*?/
        )

        E_task: ($) -> seq(
            $._OPERATION
            $._VALUE
        )

# %~
#-------------------------------------------------------------------------------
# ~~% OPERATION            {OK}

        _OPERATION: ($) -> choice(
            $._generalOp
            $._mathOp
            $._logicOp
        )


        _generalOp: ($) ->
            $.O_set
            $.O_idea
            $.O_ofType
            $.O_inherit
            $.O_function


        O_set: ($) -> /\s*?:\s*?/
        O_idea: ($) -> /\s*?~>.*?/
        O_ofType: ($) -> /\s*?::\s*?/
        O_inherit: ($) -> /\s*?<:\s*?/
        O_function: ($) -> /\s*?->\s*?/

        _mathOp: ($) ->
            $.O_add
            $.O_substract
            $.O_multiply
            $.O_power
            $.O_divide
            $.O_floor
            $.O_modulus

        O_add: ($) ->/\s*?\+\s*?/
        O_substract: ($) -> /\s*?\-\s*?/
        O_multiply: ($) -> /\s*?\*\s*?/
        O_power: ($) -> /\s*?\*\*\s*?/
        O_divide: ($) -> /\s*?\/\s*?/
        O_floor: ($) -> /\s*?\/\/\s*?/
        O_modulus: ($) -> /\s*?%\s*?/

        _logicOp: ($) -> choice(
            $.O_equals
            $.O_less
            $.O_more
            $.O_lesseq
            $.O_moreeq
            $.O_noteq
        )

        O_equals: ($) -> /\s*?=\s*?/
        O_less: ($) -> /\s*?<\s*?/
        O_more: ($) -> /\s*?>\s*?/
        O_lesseq: ($) -> /\s*?<=\s*?/
        O_moreeq: ($) -> /\s*?>=\s*?/
        O_noteq: ($) -> /\s*?!=\s*?/

# %~
#-------------------------------------------------------------------------------
# ~~% VALUE            {OK}

        _VALUE: ($) -> choice(
            $.V_var
            $.V_type
            $.V_num
            $.V_str
            $.V_list
            $.V_dict
        )

        V_type: ($) -> choice(
            $.type_regular
            $.type_custom
        )
        type_regular: ($) -> choice(
            /VAR/
            /NUM/
            /STR/
            /LIST/
            /DICT/
            /TYPE/
        )
        type_custom: ($) -> /[A-z][a-z0-9_]+?/

        V_var: ($) -> /\$\w+?/
        V_num: ($) -> /\d+?(\.\d+?)?/
        V_str: ($) -> /".*?"|'.*?'|`.*?`/
        V_list: ($) -> seq(
            /\[/
            $._VALUE
            repeat(
                seq(
                    /\s*?,\s*?/
                    $._VALUE
                )
            )
            /\]/
        )
        V_dict: ($) -> seq(
            /\{/
            $._VALUE
            optional($.EXP)
            repeat(seq(
                    /\s*?,\s*?/
                    $._VALUE
                    optional($.EXP)
            ))
            /\}/
        )

# %~
#-------------------------------------------------------------------------------
# ~~% STRUCT            {OK}

        _STRUCT: ($) -> seq(
            $.S_root
            $.S_spec
            optional($.S_next)
        )

        S_root: ($) -> /\[[A-Z0-9_]+?\]/
        S_node: ($) -> /\[[A-z][a-z0-9_]+?\]/
        S_edge: ($) -> /\+\-\-\s/

        S_spec: ($) -> seq(
            optional($.O_set)
            optional($.O_ofType)
            optional($.O_inherit)
            optional($.O_idea)
            $.EOL
        )

        S_sibling: ($) -> seq(
            $.S_edge
            $.S_node
            $.S_spec
            optional($.S_next)
        )

        S_child: ($) -> seq(
            $.TAB
            $.S_edge
            $.S_node
            $.S_spec
            optional(seq(
                $.TAB
                $.S_next
            ))

        )

        S_next: ($) -> choice(
                $.S_sibling
                $.S_child
        )

# %~

)

###
coffee -cw ./
export PATH=$PATH:./node_modules/.bin
tree-sitter generate && tree-sitter parse TARGET
###
