# UA & RU for X11: 2-in-1 keyboard layout

## IDEA
I got tired of constantly switching between 2 almost identical keyboard layouts, so I've merged them into one. At first, I've replaced the rarely used `` ` `` (grave) symbol with the letter `` ы `` in standard Ukrainian layout, which made it suitable for almost 90% of Russian layout use cases. Later, the other useful changes were made and I still update UAxRU11 everytime I find something to improve.

## NOTES
* In order not to increase the number of key combinations I've chosen the way of sacrificing not/rarely used symbols, so layout is ought to be a little odd & hard to use at first.
* Also, some capital letters were removed, so I don't recommend to use this layout without global uppercase shortcut or etc.

## CHANGES
* `` ` ``  &rarr;  `` ы ``
* `` Ь ``  &rarr;  `` ъ ``
* `` ґ ``  &rarr;  `` \ ``
* `` Ґ ``  &rarr;  `` | ``

## INSTALLATION

Replace `` ua `` file at one of these addresses...

* /etc/X11/xkb/symbols
* /usr/share/X11/xkb/symbols

... with the one that's provided here and reboot the system.

